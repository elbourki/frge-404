import React from "react";
import { Modal } from "@forge/bridge";

function App() {
  return (
    <button
      onClick={() =>
        new Modal({
          resource: "modal",
        }).open()
      }
    >
      Open modal
    </button>
  );
}

export default App;
